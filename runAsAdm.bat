rem 在win7下右击此脚本，然后以管理员身份运行
rem 需要 把python.exe所在的目录加入环境变量

@echo off  
if not "%1" == "uac" (  
    echo 申请UAC权限...
    goto GetUAC  
) else ( goto DO )  

:GetUAC  
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"  
    echo UAC.ShellExecute "%~s0", "uac", "", "runas", 1 >> "%temp%\getadmin.vbs"  

    "%temp%\getadmin.vbs"  
    exit /B  
  
:DO  
    if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )  
    pushd "%CD%"  
    CD /D "%~dp0" 
        

python.exe %~dp0updateHosts.py
pause