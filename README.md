hosts
=====

屏蔽一些跟踪网站，烦人广告

一键切换不同的host映射 
> 比如在测试阶段，可以将 example.com指向本机、同事的服务器或者公网的测试服务器

脚本是用python写的,目前需要python2.7环境
等程序稳定了会将它编译成exe文件发布出来

###使用方法:

1. clone一份代码到本地
2. 运行 updateHosts.py
	
		win7用户右击runAsAdm.bat以管理员身份运行
		也可发送快捷方式到桌面->右击属性->快捷方式->高级->用管理员身份运行 打勾 确定
        或者双击导入ruanAsAdm.reg，以后右击.py就有以管理员身份运行的选项了
###文件说明:


- /hosts.block/domain.txt 需要屏蔽的网址 现在是按照网站来区分的

- /whitehost.txt 白名单（优先级比/hosts.block/domain.txt高）在此文件中的域名不加入hosts屏蔽

- /hosts/domain.txt 此目录下的文件会原样合并到文件中



###计划
1. <del>支持多系统(xp、win7,linux)</del>
2. <del>白名单 (done)</del>
3. <del>增加自定义映射 /hosts/domain.txt (done 13.6.18)</del>
5. 不依赖git更新屏蔽库
6. 可指定配置文件（指定只需要/hosts/下哪些文件合并到hosts文件中，而剩下的忽略）
7. <del>在配置文件中可指定使用远程host数据文件</del>
8. <del>支持设置多个 webhosts</del>
9. 远程hosts文件可自动定时更新
10. <del>支持变量功能 {a.com}  b.com </del>